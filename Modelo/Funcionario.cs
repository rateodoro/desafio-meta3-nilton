﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class Funcionario
    {
        public int IdFuncionario { get; set; }
        public int IdPessoaFisica { get; set; }
        public int IdFuncao { get; set; }
        public decimal Salario { get; set; }
        public DateTime DataAdmissao { get; set; }
        public DateTime DataDemissao { get; set; }
        public bool Ativo { get; set; }
    }
}
