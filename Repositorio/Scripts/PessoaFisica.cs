﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio.Scripts
{
    public class PessoaFisica
    {
        public const string ObterListaPessoaFisica = @"SELECT	pf.Id,
                                                                pf.Cpf,
                                                                pf.Nome,
                                                                pf.DataNascimento,
                                                                pf.Email,
                                                                pf.TelefoneFixo,
                                                                pf.TelefoneCelular,
                                                                pf.Sexo
                                                            FROM PessoaFisica pf";

        public const string InserirPessoaFisica = @"INSERT INTO PessoaFisica (Cpf, Nome, DataNascimento, Email, TelefoneFixo, TelefoneCelular, Sexo)
                                                    VALUES (@Cpf, @Nome, @DataNascimento, @Email, @TelefoneFixo, @TelefoneCelular, @Sexo)";

        public const string AtualizarPessoaFisica = @"UPDATE PessoaFisica set 
                                                        Cpf = @Cpf,
                                                        Nome = @Nome,
                                                        DataNascimento = @DataNascimento,
                                                        Email = @Email,
                                                        TelefoneFixo = @TelefoneFixo,
                                                        TelefoneCelular = @TelefoneCelular,
                                                        Sexo = @Sexo
                                                        FROM PessoaFisica
                                                        WHERE Id = @IdPessoaFisica";

        public const string DeletarPessoaFisica = @"DELETE FROM PessoaFisica WHERE Id = @IdPessoaFisica ";

        public const string ObterPessoaFisicaPorId = @"SELECT	
                                                            pf.Id,
                                                            pf.Cpf,
                                                            pf.Nome,
                                                            pf.DataNascimento,
                                                            pf.Email,
                                                            pf.TelefoneFixo,
                                                            pf.TelefoneCelular,
                                                            pf.Sexo
                                                    FROM PessoaFisica pf
                                                    WHERE pf.Id = @IdPessoaFisica";

    }
}
