﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class ConexaoBanco
{
    public static SqlConnection Conectar()
    {
        //SqlTransaction transacao;
        var conexao = ConfigurationManager.ConnectionStrings["GestaoContextoBD"].ConnectionString;
        var objetoConexao = new SqlConnection(conexao);

        objetoConexao.Open();

        //transacao = objetoConexao.BeginTransaction();

        // Monta o Objeto
        var ObjetoCommand = new SqlCommand
        {
            Connection = objetoConexao
            //Transaction = transacao
        };

        return ObjetoCommand.Connection;
    }
}
