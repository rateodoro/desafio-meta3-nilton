﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public static class ControleUsuario
    {
        public static Usuario ObterUsuarioPorLogin(string login)
        {
            return Repositorio.UsuarioRepositorio.ObterUsuarioPorLogin(login);
        }

        public static bool ValidaLogin(string login, string senha)
        {
            return Repositorio.UsuarioRepositorio.ValidaLogin(login, senha);
        }

        public static List<Usuario> ObterListaUsuarios()
        {
            return Repositorio.UsuarioRepositorio.ObterListaUsuario();
        }

        public static Usuario ObterUsuarioPorId(int id)
        {
            return Repositorio.UsuarioRepositorio.ObterUsuarioPorId(id);
        }

        public static void AtualizarUsuario(Usuario usuario)
        {
            Repositorio.UsuarioRepositorio.AtualizarUsuario(usuario);
        }

        public static void DeletarUsuario(int id)
        {
            Repositorio.UsuarioRepositorio.DeletarUsuario(id);
        }

        public static void InserirUsuario(Usuario usuario)
        {
            Repositorio.UsuarioRepositorio.InserirUsuario(usuario);
        }
    }
}
