﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo;

namespace Negocio
{
    public static class ControleFuncionario
    {
        public static List<Funcionario> ObterListaFuncionario()
        {
            return Repositorio.FuncionarioRepositorio.ObterListaFuncionario();
        }

        public static void InserirFuncionario(Funcionario Funcionario)
        {
            Repositorio.FuncionarioRepositorio.InserirFuncionario(Funcionario);
        }

        public static void AtualizarFuncionario(Funcionario Funcionario)
        {
            Repositorio.FuncionarioRepositorio.AtualizaFuncionario(Funcionario);
        }

        public static void DeletarFuncionario(int id)
        {
            Repositorio.FuncionarioRepositorio.DeletarFuncionario(id);
        }

        public static Funcionario ObterFuncionarioPorId(int id)
        {
            return Repositorio.FuncionarioRepositorio.ObterFuncionarioPorId(id);
        }

        public static List<Funcao> ObterListaFuncao()
        {
            return Repositorio.FuncionarioRepositorio.ObterListaFuncao();
        }
    }
}
